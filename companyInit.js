companyInit = (function() {
  
  let isCalled = false;

  function companyInit() {
    if(isCalled) {
      return;
    }
    isCalled = true;
    
    window.company = new Company('Yojji', 6);
    company.registerUserCreateCallback((user) => renderUser(user));
    company.registerUserCreateCallback((user) => popNotification(`${user.name} ${user.lastname} just joined`));
    company.registerUserUpdateCallback((user) => renderUserUpdate(user));
    company.registerUserUpdateCallback((user) => {
      if(typeof user === 'number') {
        popNotification(`User with id "${user}" was deleted`);
        return;
      }
      popNotification(`${user.name} ${user.lastname} just updated`);
    });
    company.registerNoSpaceNotifyer((message) => popNotification(message));

    renderCompany(company);

    admin = Company.createSuperAdmin(company);

    admin.createUser('Ira', 'Petrova');
    userVanya = admin.createUser('Vanya', 'Petrova');
    admin.createUser('Dima', 'Vasiliev').role = 'just user';
    admin.createUser('Anya', 'Petrova').role = 'just user';
    userAlina = admin.createUser('Alina', 'Petrova');

    admin.deleteUser(userVanya.id);

    let table = document.querySelector('.table');
    table.addEventListener('click', event => {
      const target = event.target;
      if(target.classList.contains('delete-btn')) {
        const targetId = target.getAttribute('data-btn-id');
        admin.deleteUser(+targetId)
      }
    })
  }

  return companyInit;

})();
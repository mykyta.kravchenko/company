var Company = (function() {
  const role = Symbol('role');
  const token = Symbol('token');
  const usersArray = Symbol('usersArray');
  const TOKEN  = 'Secret token';
  const password = Symbol('password');
  const createUserCallbacks = Symbol('create');
  const updateUserCallbacks = Symbol('update');
  const noSpaceCallbacks = Symbol('noSpace');
  const companySymbol = Symbol('company');
  let idGen = 0;

  class User {
    #name;
    #lastname;
    #id;

    constructor(name, lastname, isAdmin = false) {
      this.#name = name;
      this.#lastname = lastname;
      this.#id = idGen++;

      if(isAdmin !== true) {
        return;
      }

      this[role] = "superUser";
      this[token] = TOKEN;

      this.createUser = function(name, lastName) {
        if(prompt('Enter password') !== this[password]) {
          throw new TypeError('The password is incorrect');
          return;
        }

        if(this[token] !== TOKEN){
          throw new TypeError('You don\'t have the rights');
          return;
        }

        let company = this[companySymbol];

        if(company[usersArray].length === company.maxSize) {
          throw new TypeError('The company is full!')
          return;
        }

        let newUser = new User(name, lastName);
        newUser[updateUserCallbacks] = company[updateUserCallbacks];
        company[usersArray].push(newUser);
        if(company[createUserCallbacks].length) {
          company[createUserCallbacks].forEach(callback => callback(newUser))
        }
        
        if(company[usersArray].length === company.maxSize) {
          if(company[noSpaceCallbacks].length) {
            company[noSpaceCallbacks].forEach(callback => callback('The company is full!'))
          }
        }
        return newUser;
      }


      this.deleteUser = function(id) {
        if(prompt('Enter password') !== this[password]) {
          throw new TypeError('The password is incorrect');
          return;
        }
        if(this[token] !== TOKEN) {
          throw new TypeError('You don\'t have the rights');
          return;
        }

        let company = this[companySymbol];

        let userIndex = company[usersArray].findIndex(user => {
          return user.id === id;
        });
        if(userIndex === -1) {
          throw new TypeError('User with this id doesn\'t exist');
          return;
        }
        if(userIndex === 0) {
          throw new TypeError('Super admin can\'t be deleted');
          return;
        }

        company[usersArray].splice(userIndex, 1);
        
        if(company[updateUserCallbacks].length) {
          company[updateUserCallbacks].forEach(callback => callback(id))
        }
      }
    }

    get name() {
      return this.#name;
    }

    get lastname() {
      return this.#lastname;
    }

    get role() {
      return this[role];
    }

    get id() {
      return this.#id;
    }

    set name(firstName) {
      this.#name = firstName;
      if(this[updateUserCallbacks].length){
        this[updateUserCallbacks].forEach(cb => cb(this));
      }
    }

    set lastname(lastName) {
      this.#lastname = lastName;
      if(this[updateUserCallbacks].length){
        this[updateUserCallbacks].forEach(cb => cb(this));
      }
    }

    set role(userRole) {
      this[role] = userRole;
      if(this[updateUserCallbacks].length){
        this[updateUserCallbacks].forEach(cb => cb(this));
      }
    }
  }

  return class  Company {
    constructor(name, maxSize) {
      this.name = name;
      this.maxSize = maxSize;
      this[usersArray] = [];
      this[createUserCallbacks] = [];
      this[updateUserCallbacks] = [];
      this[noSpaceCallbacks] = [];
    }

    get curSize() {
      return this[usersArray].length;
    }

    getUser(id) {
      let user = this[usersArray].find(user => user.id == id);
      if(user){
        return user;
      }
      throw new TypeError('User with this id doesn\'t exist');
    }

    static createSuperAdmin(company) {
      if(!(company instanceof Company)) {
        return;
      }

      if(company[usersArray][0] && company[usersArray][0][token] === TOKEN) {
        throw new TypeError('Super admin already exists');
        return;
      }

      const admin = new User('super', 'Admin', true);
      if(company[createUserCallbacks].length) {
        company[createUserCallbacks].forEach(callback => callback(admin))
      }

      admin[password] = prompt('Create password:');

      admin[companySymbol] = company;
      admin[updateUserCallbacks] = company[updateUserCallbacks];
      company[usersArray].push(admin);
      return admin;
    }

    registerUserCreateCallback(callback) {
      this[createUserCallbacks].push(callback);
    }

    registerUserUpdateCallback(callback) {
      this[updateUserCallbacks].push(callback);
    }

    registerNoSpaceNotifyer(callback) {
      this[noSpaceCallbacks].push(callback);
    }
  }
})()
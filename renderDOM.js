function renderCompany(company){
  const companyInfo = document.createElement('main');
  companyInfo.innerHTML = `
      <div class="table-header ">
      <div class="comp-header bg-secondary">
        <h3>Company name:</h3>
        <h3 class="company-name text-info">${company.name}</h3>
      </div>
      <div class="comp-header bg-secondary">
        <h3>Max Size:</h3>
        <h3 class="company-max-size text-info">${company.maxSize}</h3>
      </div>
      <div class="comp-header bg-secondary">
        <h3>Cur size:</h3>
        <h3 class="company-cur-size text-info">${company.curSize}</h3>
      </div>
    </div>

    <table class="table table-bordered">
      <thead class="thead-dark"> 
        <tr>
          <th scope="col" class="number-col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Lastname</th>
          <th scope="col">Role</th>
          <th scope="col">Id</th>
          <th scope="col" class="delete-col">Delete</th>
        </tr>
      </thead>
      <tbody class="tbody">
        <template>
          <tr>
            <th scope="row" class="number-col number-list">1</th>
            <td class="name">{name}</td>
            <td class="lastname">{lastname}</td>
            <td class="role">{role}</td>
            <td class="id">{id}</td>
            <td class="delete-col"><i class="fas fa-times delete-btn"></i></td>
          </tr>
        </template>
      </tbody>
    </table>
  `;
  
  document.body.appendChild(companyInfo);
}

let number = 1;

function renderUser(user) {
  if(user.id === 0) {
    return;
  }

  let template = document.querySelector('template');
  let tempRow = template.content.querySelector('tr');
  let tempRowClone = tempRow.cloneNode(true);
  tempRowClone.setAttribute('data-id', user.id);
  tempRowClone.querySelector('.number-col').textContent = number++;
  tempRowClone.querySelector('.name').textContent = user.name;
  tempRowClone.querySelector('.lastname').textContent = user.lastname;
  tempRowClone.querySelector('.role').textContent = user.role;
  tempRowClone.querySelector('.id').textContent = user.id;
  tempRowClone.querySelector('i').setAttribute('data-btn-id', user.id);
  document.querySelector('tbody').appendChild(tempRowClone);
  const curSizeElement = document.querySelector('.company-cur-size');
  curSizeElement.textContent = company.curSize;
}

function renderUserUpdate(user) {

  if(typeof user === 'number') {
    let buttonId = document.querySelector(`i[data-btn-id="${user}"]`);
    if(!buttonId) {
      throw new TypeError('Error on deleting user');
    }
    let trToRemove = buttonId.parentElement.parentElement;
    trToRemove.remove();
    const curSizeElement = document.querySelector('.company-cur-size');
    curSizeElement.textContent = company.curSize;
    updateNumberList();
    return;
  }
  
  let rowToUpdate = document.querySelector(`tr[data-id="${user.id}"]`);
  rowToUpdate.querySelector('.name').textContent = user.name;
  rowToUpdate.querySelector('.lastname').textContent = user.lastname;
  rowToUpdate.querySelector('.role').textContent = user.role;
}




function popNotification(notificationText) {
  const notifyElement = document.createElement('div');
  Object.assign(notifyElement.style, {
    position: 'absolute',
    top: '20px',
    right: '40px',
    background: '#fff',
    zIndex: '2',
    transition: '.5s',
  })
  notifyElement.innerHTML = `
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Company notify</h5>
      </div>
      <div class="modal-body">
        <p>${notificationText}</p>
      </div>
    </div>
  `;
  document.body.appendChild(notifyElement);
  setTimeout(() => {
    notifyElement.remove()
  }, 5000);
}


function updateNumberList() {
  const listOfTableHeaders = document.querySelectorAll('.number-list');
  listOfTableHeaders.forEach((el, index) => {
    el.textContent = index + 1;
    number = index + 2;
  })
}